
# 2013-02-07 johnpfeiffer
# does not support multi value properties

import os


OID_IDENTIFIER = 'storagegateway.storageGatewayOid='
SIGNEDHASH_IDENTIFIER = 'storagegateway.storageGatewaySignedHash='
IBM_USERNAME_IDENTIFIER = 'storagegateway.nirvanixrest.username='
IBM_PASSWORD_IDENTIFIER = 'storagegateway.nirvanixrest.password='
IBM_APPNAME_IDENTIFIER = 'storagegateway.nirvanixrest.appName='
IBM_APPKEY_IDENTIFIER = 'storagegateway.nirvanixrest.appKey='


#listing = os.listdir( '/var/lib/tomcat6/webapps/storagegateway/WEB-INF' )
#print '%s' % ( listing )


def get_property( identifier , filename ):
    """ get the value of the last occurrence of a line starting with identifier """
    property = None
    source = open( filename )
    for line in source:
        line = line.strip()
        if line.startswith( identifier ):
            oid_line_list = line.split( '=' )
            if len( oid_line_list ) > 1:
                property = oid_line_list[ 1 ]
    return property

def set_property( identifier , new_value , filename ):
    out_file = open( filename + '.new' , 'w' )
    source = open( filename )
    for line in source:
        line = line.strip()
        if line.startswith( identifier ):
            print 'updating %s to %s%s' % ( line , identifier , new_value )
            out_file.write( identifier + new_value + os.linesep )
        else:
            out_file.write( line + os.linesep )
    out_file.close()
    source.close()


# MAIN ###############################

filename = '/var/lib/tomcat6/webapps/storagegateway/WEB-INF/app.properties'

print '%s' % get_property( OID_IDENTIFIER , filename )
print '%s' % get_property( SIGNEDHASH_IDENTIFIER , filename )
print '%s' % get_property( IBM_USERNAME_IDENTIFIER , filename )
print '%s' % get_property( IBM_PASSWORD_IDENTIFIER , filename )
print '%s' % get_property( IBM_APPNAME_IDENTIFIER , filename )
print '%s' % get_property( IBM_APPKEY_IDENTIFIER , filename )

set_property( IBM_APPKEY_IDENTIFIER , 'new-key' , filename )


print '%s' % get_property( IBM_APPKEY_IDENTIFIER , filename + '.new' )

