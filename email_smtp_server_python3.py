#!/usr/bin/env python3

# python email_smtp_server_python3.py
# https://docs.python.org/3/library/smtpd.html
import smtpd
import asyncore


class EmailSMTPServer(smtpd.SMTPServer):

    def process_message(self, peer, mailfrom, rcpttos, data):
        print(repr(peer))
        print(repr(mailfrom))
        print(repr(rcpttos))
        print(repr(data))


if __name__ == "__main__":
    # 0.0.0.0 allows all client connections, change it to localhost for local development machine access only
    smtp_server = EmailSMTPServer(('0.0.0.0', 2525), None)
    try:
        asyncore.loop()
    except KeyboardInterrupt:
        smtp_server.close()
