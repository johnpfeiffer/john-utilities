import os
import argparse


class ListDirectory(object):
    """ focused on a single directory with a more convenient API interface (and less performant than os.walk())
        assumes the caller understands the return are only the file names (since they passed the start path in...)
        and the caller can do their own post processing (i.e. sorting)
    """

    def list_files_only(self, start, filter_func=None, filter_params=None):
        (subdirectories, files) = self._listing(start, filter_func=filter_func, filter_params=filter_params)
        return files

    def list_files_only_including_hidden(self, start, filter_func=None, filter_params=None):
        (subdirectories, files) = self._listing(start, list_hidden=True, filter_func=filter_func,
                                                filter_params=filter_params)
        return files

    def list_directories_only(self, start, filter_func=None, filter_params=None):
        (subdirectories, files) = self._listing(start, list_directories=True, filter_func=filter_func,
                                                filter_params=filter_params)
        return subdirectories

    def list_directories_only_including_hidden(self, start, filter_func=None, filter_params=None):
        (subdirectories, files) = self._listing(start, list_directories=True, list_hidden=True, filter_func=filter_func,
                                                filter_params=filter_params)
        return subdirectories

    def list_files_and_directories(self, start, filter_func=None, filter_params=None):
        (files, subdirectories) = self._listing(start, list_directories=True, filter_func=filter_func,
                                                filter_params=filter_params)
        return files, subdirectories

    def list_files_and_directories_including_hidden(self, start, filter_func=None, filter_params=None):
        (files, subdirectories) = self._listing(start, list_directories=True, list_hidden=True, filter_func=filter_func,
                                                filter_params=filter_params)
        return files, subdirectories

    # TODO: edge case for listing only hidden dirs with all files or vice versa!?!?

    def _listing(self, start, list_directories=False, list_hidden=False, filter_func=None, filter_params=None):
        """
        :param start:
        :param list_directories:
        :param list_hidden:
        :param filter_func: e.g. startswith
        :param filter_params: e.g. foo
        :return:
        """
        # TODO: handle edge case of inverted filter list, i.e. all the files that do NOT startswith('foo')
        directory_list = list()
        file_list = list()
        for current in os.listdir(start):
            if not list_directories and os.path.isdir(current):
                continue
            if not list_hidden and current[0] == '.':
                continue
            if filter_func == 'startswith':
                if not current.startswith(filter_params):
                    continue
            if filter_func == 'endswith':
                if not current.endswith(filter_params):
                    continue
            if filter_func == 'in':
                if not filter_params in current:
                    continue

            full_path = os.path.join(start, current)
            if os.path.isdir(full_path):
                directory_list.append(current)
            if os.path.isfile(full_path):
                file_list.append(current)
        return directory_list , file_list


if __name__ == '__main__':

    # https://docs.python.org/3/library/argparse.html
    parser = argparse.ArgumentParser()
    # default behavior is to just show files and directories (no hidden, same as ls -l)
    parser.add_argument('--start', help='the directory to list', required=True)
    parser.add_argument('--hidden', help='show hidden directories or files', action='store_true')
    parser.add_argument('--filter', help='python function to apply')
    parser.add_argument('--filterparams', help='parameter(s) for the python filter function')
    type_group = parser.add_mutually_exclusive_group()
    type_group.add_argument('--dirsonly', help='only show directories', action='store_true')
    type_group.add_argument('--filesonly', help='only show files', action='store_true')
    args = parser.parse_args()

    result = None
    l = ListDirectory()

    if args.dirsonly:
        if args.hidden:
            result = l.list_directories_only_including_hidden(args.start, filter_func=args.filter,
                                                              filter_params=args.filterparams)
        else:
            result = l.list_directories_only(args.start, filter_func=args.filter, filter_params=args.filterparams)

    if args.filesonly:
        if args.hidden:
            result = l.list_files_only_including_hidden(args.start, filter_func=args.filter,
                                                        filter_params=args.filterparams)
        else:
            result = l.list_files_only(args.start, filter_func=args.filter, filter_params=args.filterparams)

    if not args.dirsonly and not args.filesonly:
        if args.hidden:
            result = l.list_files_and_directories_including_hidden(args.start, filter_func=args.filter,
                                                                   filter_params=args.filterparams)
        else:
            result = l.list_files_and_directories(args.start, filter_func=args.filter, filter_params=args.filterparams)

    if isinstance(result, tuple):
        for i in sorted(result[0]):
            print(i)
        if result[0]:
            print('\n')
        for i in sorted(result[1]):
            print(i)
    else:
        for i in sorted(result):
            print(i)

