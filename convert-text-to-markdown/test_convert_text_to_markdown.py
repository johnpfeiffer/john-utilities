# http://click.pocoo.org/6/testing/#basic-testing
# import click
# from click.testing import CliRunner
# from convert_text_to_markdown import convert_files
#
# def test_convert_text_to_markdown():
#     @click.command()
#     @click.argument('name')
#     def hello(name):
#         click.echo('Hello %s!' % name)
#
#     runner = CliRunner()
#     result = runner.invoke(hello, ['Peter'])
#     assert result.exit_code == 0
#     assert result.output == 'Hello Peter!\n'


import unittest
import tempfile

import convert_text_to_markdown

EXPECTED_SOURCE = ['foobar is fun']
EXPECTED_MULTILINE_SOURCE_WITH_TRAILING_WHITESPACE = ['first trailing whitespace \n', 'second trailing tab    ']
EXPECTED_TITLE = 'test write file'
EXPECTED_DATE = '2016-04-16 21:00:59'
EXPECTED_TAGS = 'test, line'
EXPECTED_SINGLE_TAG = 'singletag'


class TestConvertTextToMarkdown(unittest.TestCase):

    def setUp(self):
        self.text_extension = '.txt'

    def test_is_source_code_extension(self):
        for i in convert_text_to_markdown.SOURCE_CODE_EXTENSIONS:
            self.assertTrue(convert_text_to_markdown.is_source_code_extension(i))

    def test_not_source_code_extension_returns_false(self):
        for i in ['', '-', '.', 'c', '.cc']:
            self.assertFalse(convert_text_to_markdown.is_source_code_extension(i))

    def test_write_txt_file(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            file_handle, file_abs_path = tempfile.mkstemp(dir=tmpdirname)
            convert_text_to_markdown.write_file(file_abs_path, EXPECTED_SOURCE, EXPECTED_TITLE, EXPECTED_DATE,
                                                EXPECTED_TAGS, self.text_extension)
            with open(file_abs_path) as f:
                self.assertEqual('Title: {}\n'.format(EXPECTED_TITLE), f.readline())
                self.assertEqual('Date: {}\n'.format(EXPECTED_DATE), f.readline())
                self.assertEqual('Tags: {}\n'.format(EXPECTED_TAGS), f.readline())
                self.assertEqual('\n', f.readline())
                source_line = EXPECTED_SOURCE[0]
                self.assertEqual('{}'.format(source_line), f.readline())
                # assert end of file, https://docs.python.org/3/library/io.html#io.TextIOBase.readline
                self.assertEqual('', f.readline())

    def test_write_source_code_file(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            file_handle, file_abs_path = tempfile.mkstemp(dir=tmpdirname)
            for source_code_extension in convert_text_to_markdown.SOURCE_CODE_EXTENSIONS :
                convert_text_to_markdown.write_file(file_abs_path, EXPECTED_SOURCE, EXPECTED_TITLE, EXPECTED_DATE,
                                                    EXPECTED_TAGS, source_code_extension)
                with open(file_abs_path) as f:
                    self.assertEqual('Title: {}\n'.format(EXPECTED_TITLE), f.readline())
                    self.assertEqual('Date: {}\n'.format(EXPECTED_DATE), f.readline())
                    self.assertEqual('Tags: {}\n'.format(EXPECTED_TAGS), f.readline())
                    self.assertEqual('\n', f.readline())
                    source_line = EXPECTED_SOURCE[0]
                    self.assertEqual('    {}\n'.format(source_line), f.readline())
                    self.assertEqual('', f.readline())

    def test_write_source_code_file_no_trailing_whitespace_and_single_tag(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            file_handle, file_abs_path = tempfile.mkstemp(dir=tmpdirname)
            convert_text_to_markdown.write_file(file_abs_path, EXPECTED_MULTILINE_SOURCE_WITH_TRAILING_WHITESPACE,
                                                EXPECTED_TITLE, EXPECTED_DATE, EXPECTED_SINGLE_TAG, '.py')
            with open(file_abs_path) as f:
                self.assertEqual('Title: {}\n'.format(EXPECTED_TITLE), f.readline())
                self.assertEqual('Date: {}\n'.format(EXPECTED_DATE), f.readline())
                self.assertEqual('Tags: {}\n'.format(EXPECTED_SINGLE_TAG), f.readline())
                self.assertEqual('\n', f.readline())
                first_source_line = EXPECTED_MULTILINE_SOURCE_WITH_TRAILING_WHITESPACE[0].rstrip()
                self.assertEqual('    {}\n'.format(first_source_line), f.readline())
                second_source_line = EXPECTED_MULTILINE_SOURCE_WITH_TRAILING_WHITESPACE[1].rstrip()
                self.assertEqual('    {}\n'.format(second_source_line), f.readline())
                self.assertEqual('', f.readline())
