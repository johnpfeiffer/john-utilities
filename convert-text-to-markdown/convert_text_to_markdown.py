#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-
# TODO: extract file listing into a common helper class so this class can focus on just "conversion"
# TODO: refactor to allow for unit testing

import os
import click
# http://click.pocoo.org/6/ , https://github.com/mitsuhiko/click/blob/master/examples/inout/inout.py

from file_metadata import get_title, get_date, generate_tags


SOURCE_CODE_EXTENSIONS = ['.c', '.cpp', '.py', '.java', '.bat', '.js', '.php']


def is_source_code_extension(extension):
    if extension in SOURCE_CODE_EXTENSIONS:
        return True
    else:
        return False


def write_file(target_path, source, title, modified_date, tags, extension):
    with open(target_path, 'w') as f:
        f.write('Title: ' + title + '\n')
        f.write('Date: ' + modified_date + '\n')
        f.write('Tags: ' + tags + '\n')
        f.write('\n')
        for line in source:
            # make all source code code snippets in markdown
            if is_source_code_extension(extension):
                f.write('    {}\n'.format(line.rstrip()))
            else:
                f.write(line)


@click.command()
@click.argument('indir', nargs=1, required=True, type=click.Path(exists=True))
@click.argument('outdir', nargs=1, required=True, type=click.Path(exists=True))
def convert_files(indir, outdir):
    """ This script reads a directory of text files from INDIR and outputs them as .md files to OUTDIR
    """
    click.echo('Getting .txt or {} files from {} and outputting .md (markdown) to {}'.format(SOURCE_CODE_EXTENSIONS, indir, outdir))
    for i in sorted(os.listdir(indir)):
        # https://docs.python.org/3/library/os.path.html#module-os.path
        full_path = os.path.join(indir, i)
        if os.path.isfile(full_path):
            filename = os.path.basename(full_path)
            extension = os.path.splitext(filename)[1]
            if extension == '.txt' or is_source_code_extension(extension):
                click.echo('{}'.format(full_path))
                filename_without_extension = os.path.splitext(filename)[0]
                title = get_title(filename_without_extension)
                modified_date = get_date(full_path)
                tags = generate_tags(full_path, title)
                # DEBUG print(title, modified_date, tags)
                target_path = os.path.join(outdir, filename_without_extension + '.md')
                with open(full_path) as source:
                    write_file(target_path, source, title, modified_date, tags, extension)


if __name__ == '__main__':
    convert_files()

# OUTPUT MARKDOWN EXAMPLE
# Title: ListJS: Sort, Filters, Search and more for HTML lists and tables in Javascript
# Date: 2014-08-08 21:37
# Tags: programming, javascript

