#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import datetime
import math
import os

"""
    for dirpath, directorynames, filenames in os.walk( start ):
        for f in filenames:
            file_count += 1
            search = True
            if extension and not f.endswith( extension ):
                search =  False
            if search:
                full_path = os.path.join( dirpath, f )
"""


def get_title(filename_without_extension):
    """ Title must come from the file name but replaces any dashes or underscores with spaces for readability
    :parameter filename_without_extension is just the string that is the filename (no extension aka last characters
    after the last period), not the path
    :return: title string
    """
    title = filename_without_extension.replace('-', ' ')
    title = title.replace('_', ' ')
    # title = title[0].upper() + title[1:] # uppercase first letter was not really helpful
    return '{}'.format(title)


def get_last_modified(full_path):
    """
    :param full_path: OS path to the file
    :return: integer that is the math floor (more deterministic) of the last modified timestamp
    """
    return math.floor(os.path.getmtime(full_path))


def get_date(full_path):
    """
    Return the last modified timestamp (assuming UTC) as a pretty formatted string
    :param full_path:
    :return:
    """
    modified_timestamp = get_last_modified(full_path)
    date_object = datetime.datetime.utcfromtimestamp(modified_timestamp)
    date_string = date_object.strftime("%Y-%m-%d %H:%M:%S")
    return '{}'.format(date_string)


def generate_tags(full_path, title):
    """ Categorizing a file by using the name of the containing directory and the "title"
    :param full_path: the absolute path of the file
    :param title: the title of the contents (usually generated from the filename earlier)
    :return: all lowercase string of unique comma separated tags (sorted by alphabet)
    """
    tags_list = title.split(' ')
    directory_path = os.path.dirname(full_path)
    directory_name = os.path.basename(directory_path)
    tags_list.append(directory_name)
    tags_set = set(tags_list)
    sorted_tag_list = sorted(list(tags_set))
    tags_string = ', '.join(sorted_tag_list)
    return tags_string.lower().strip()
