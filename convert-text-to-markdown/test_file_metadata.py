#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import unittest
import math
import os
import time
import tempfile

import file_metadata


class TestFileMetadata(unittest.TestCase):

        def test_get_title_single_word(self):
            title = file_metadata.get_title('oneword')
            self.assertEqual('oneword', title)

        def test_get_title(self):
            title = file_metadata.get_title('space a-b__cfoobar')
            self.assertEqual('space a b  cfoobar', title)

        def test_get_title_only_escapes_dashes_underscores(self):
            title = file_metadata.get_title('a.-b_._c,d~!@#$%^&*()+=\/<>')
            self.assertEqual('a. b . c,d~!@#$%^&*()+=\/<>', title)

        def test_get_title_empty(self):
            title = file_metadata.get_title('')
            self.assertEqual('', title)

        def test_get_last_modified(self):
            with tempfile.TemporaryDirectory() as tmpdirname:
                # https://docs.python.org/3/library/tempfile.html#tempfile.mkstemp
                file_handle, file_abs_path = tempfile.mkstemp(dir=tmpdirname)
                self.assertEqual(math.floor(time.time()), file_metadata.get_last_modified(file_abs_path))
                # https://docs.python.org/2/library/os.html#os.utime
                os.utime(file_abs_path, (0, 0))
                self.assertEqual(0, file_metadata.get_last_modified(file_abs_path))
                os.utime(file_abs_path, (1228262400, 1228262400))
                self.assertEqual(1228262400, file_metadata.get_last_modified(file_abs_path))

        def test_get_date(self):
            with tempfile.TemporaryDirectory() as tmpdirname:
                file_handle, file_abs_path = tempfile.mkstemp(dir=tmpdirname)
                os.utime(file_abs_path, (0, 0))
                self.assertEqual('1970-01-01 00:00:00', file_metadata.get_date(file_abs_path))
                os.utime(file_abs_path, (1228262400, 1228262400))
                self.assertEqual('2008-12-03 00:00:00', file_metadata.get_date(file_abs_path))

        def test_generate_tags(self):
            with tempfile.TemporaryDirectory() as tmpdirname:
                directory_name = os.path.basename(tmpdirname)
                filename = 'abctemp'
                file_abs_path = os.path.join(tmpdirname, filename)
                with open(file_abs_path, 'w') as f:
                    f.write('abc')
                single_word_title = file_metadata.get_title(filename)
                self.assertEqual(single_word_title, filename)
                tags = file_metadata.generate_tags(file_abs_path, single_word_title)
                # empirically tmpdirname begins with "tmp"
                self.assertEqual('{}, {}'.format(single_word_title, directory_name), tags)

        def test_generate_tags_multiword_title(self):
            with tempfile.TemporaryDirectory() as tmpdirname:
                directory_name = os.path.basename(tmpdirname)
                filename = 'abctemp_barfoo-foobar_z'
                file_abs_path = os.path.join(tmpdirname, filename)
                with open(file_abs_path, 'w') as f:
                    f.write('abc')
                tags = file_metadata.generate_tags(file_abs_path, file_metadata.get_title(filename))
                # empirically tmpdirname begins with "tmp"
                self.assertEqual('abctemp, barfoo, foobar, {}, z'.format(directory_name), tags)
