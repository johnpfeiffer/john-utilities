#!/usr/bin/env python2

# python2 -m smtpd -n -c DebuggingServer localhost:2525
# python email_client_python2.py  # does not work against a python3 -m smtpd server
# https://docs.python.org/2/library/smtplib.html
# https://docs.python.org/2/library/email-examples.html

import smtplib
from email.mime.text import MIMEText


class EmailClient(object):

    def send(self, to_email, from_email, subject, message_text, smtp_host='localhost', smtp_port=25):
        s = None
        try:
            msg = MIMEText(message_text)
            msg['From'] = from_email
            msg['To'] = to_email
            msg['Subject'] = subject
            recipients = list(msg['To'])
            s = smtplib.SMTP(host=smtp_host, port=smtp_port)
            s.sendmail(msg['From'], recipients, msg.as_string())
        except Exception as error:
            return str(error)
        finally:
            if s:
                s.quit()
        return 'success'


if __name__ == "__main__":

    c = EmailClient()
    result = c.send(to_email='john@example.com', from_email='me@example.com', subject='my email subject',
                    message_text='message body goes here', smtp_host='localhost', smtp_port=2525)
    print(result)
    # Check if the server is running/accessible and port number: [Errno 111] Connection refused