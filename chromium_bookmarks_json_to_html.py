#!/usr/bin/env python3
# convert from ~/.config/chromium/Default/Bookmarks (json) to an import compatible bookmarks.html

import json
import sys

FOLDER_TYPE = 'folder'
URL_TYPE = 'url'


class Url(object):

    def __init__ (self):
        self.date_added = None
        self.url_id = None
        self.name = None
        self.type = URL_TYPE
        self.url = ''


class Folder(object):

    def __init__ (self):
        self.folders = list()
        self.urls = list()
        self.date_added = None
        self.date_modified = None
        self.folder_id = None
        self.name = None
        self.type = FOLDER_TYPE

"""
{
   "checksum": "12345678cd7c6b7adc5fc6789a333a9",
   "roots": {
      "bookmark_bar": {
         "children": [ {
            "children": [ {
               "date_added": "13022920251941780",
               "id": "217",
               "name": "Example",
               "type": "url",
               "url": "https://example.com/"
            }, {
                        "type": "folder"
         }, {
            "children": [ {
               "date_added": "12952921610000000",
               "id": "37",
               "name": "Heroku Login",
               "type": "url",
               "url": "https://api.heroku.com/login?url=%2Fmyapps"
            },


    <!DOCTYPE NETSCAPE-Bookmark-file-1>
    <!-- This is an automatically generated file.
         It will be read and overwritten.
         DO NOT EDIT! -->
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
    <TITLE>Bookmarks</TITLE>
    <H1>Bookmarks</H1>
    <DL><p>
        <DT><H3 ADD_DATE="1449633644" LAST_MODIFIED="1449636120" PERSONAL_TOOLBAR_FOLDER="true">Bookmarks bar</H3>
        <DL><p>
            <DT><A HREF="http://blog.john-pfeiffer.com/" ADD_DATE="1449634633">John Pfeiffer</A>
            <DT><H3 ADD_DATE="1449636107" LAST_MODIFIED="1449636123">example</H3>
            <DL><p>
                <DT><A HREF="https://news.ycombinator.com/" ADD_DATE="1449636120"
                ICON="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABfklEQVQ4jX3Tv4oUQRAG8F/NDueurFxwiFwgohgIl5gZ+AAGBvoS5xOIXCIchqKZqRj7AocggqCIkW/gA3hGJ/5jb2baYHp2eha1h6GLqq+qvv66OtK+FpH/lPf/rVRgU7VOGNICVYwlY8NXYolKZEjKoRarNPbKplWizZgBG6KS8ged5MyS85doCtINdi4yX/aYvniSpJ5BZB5JmC148Ja9W/zGCpdvcP81s7M9Job+oppIs1Vx/JV3L7h72J/1FHcO+fiSL8c9Jo3kxgLQdSxx9ITtXa7d5MoeF65y9LiPdd0kpV6rOghWV5z85NVTbh+w+sWbZ3z7wXY1Fsg3EeneWsJiBc2MR5+IiofXqVs2gVEyUMRnwUnD5w80K7437Mxo20n3qQaT+cp25OGpkIruhVmXdEY7etD8HKdbdIVvKrt6rUAIKd9y1zHH++ekloVRvAGT83oR/0JN6AeJvlgq/P9kMJx+sBaZb9eV+kwY1DafcPmo0/TO17FxpT9lR4kM4QVHSAAAAABJRU5ErkJggg==
                ">Hacker News</A>
            </DL><p>
        </DL><p>
    </DL><p>
"""


if __name__ == '__main__':

    # TODO: parameter or ENV
    # source_bookmarks_file = sys.argv[1]
    # output_bookmarks_html_file = sys.argv[2]
    source_bookmarks_file = '/home/USERNAME/Desktop/Bookmarks'
    output_bookmarks_html_file = '/tmp/bookmarks.html'

    with open(source_bookmarks_file) as f:
        source_bookmarks = json.loads(f.read())

    if not source_bookmarks:
        print('No source bookmarks in {}'.format(source_bookmarks_file))
        sys.exit(1)

    top_level_folders = list()
    # Assumes the top level of the json/dict are only folders: bookmark_bar, other, synced, ?
    roots = source_bookmarks.get('roots')
    for k, v in roots.items():
        # TODO: try except or default value for missing keys?
        f = Folder()
        f.name = k
        f.date_added = v.get('date_added')
        f.date_modified = v.get('date_added')
        f.folder_id = v.get('id')

        for c in v.get('children'):
            if c.get('type') == FOLDER_TYPE:
                # TODO: try except or default value for missing keys?
                subfolder = Folder()
                subfolder.date_added = c.get('date_added')
                subfolder.date_modified = c.get('date_modified')
                subfolder.folder_id = c.get('id')
                subfolder.name = c.get('name')
                # TODO: this is currently a mix of folders and URLS, needs recursion
                # base case = c.get('children') is empty
                subfolder.folders = c.get('children')
                f.folders.append(subfolder)
            elif c.get('type') == URL_TYPE:
                u = Url()
                # TODO: try except or default value for missing keys?
                u.date_added = c.get('date_added')
                u.url_id = c.get('id')
                u.name = c.get('name')
                u.url = c.get('url')
                f.urls.append(u)
            else:
                raise KeyError('unexpected child type in {}'.format(f.name))

        top_level_folders.append(f)

    for f in top_level_folders:
        print(f.name)
        print('    folders: {}'.format(len(f.folders)))
        print('    urls: {}'.format(len(f.urls)))

    with open(output_bookmarks_html_file, 'w') as output:
        output.write('<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">\n')
        output.write('<TITLE>Bookmarks</TITLE>\n')
        output.write('<H1>Bookmarks</H1>\n')
        output.write('<DL>\n')

        for f in top_level_folders:
            # TODO: if f.name == bookmark_bar: PERSONAL_TOOLBAR_FOLDER="true">Bookmarks bar
            output.write('<DT><H3 ADD_DATE="{}" LAST_MODIFIED="{}">{}></H3>\n'.format(f.date_added, f.date_modified,
                                                                                      f.name))
            # <DT><H3 ADD_DATE="1449633644" LAST_MODIFIED="1449636120" PERSONAL_TOOLBAR_FOLDER="true">Bookmarks bar</H3>
            if f.folders or f.urls:
                output.write('<DL>\n')
                # TODO: recursive for subfolder in f.folders
                for u in f.urls:
                    # <DT><A HREF="https://example.com" ADD_DATE="1451966008">Example Site</A>
                    output.write('<DT><A HREF="{}" ADD_DATE="{}">{}</A>\n'.format(u.url, u.date_added, u.name))
                output.write('</DL>\n')

        output.write('</DL>\n')
