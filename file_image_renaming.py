import argparse
import os
from PIL import Image
from PIL.ExifTags import TAGS
import sys


class FileInfo(object):

    def __init__(self, full_path):
        self.full_path = full_path
        self.current_os = FileInfo.determine_os()
        self.size = None
        self.last_modified = None

    def discover_file_system_info(self):
        """ https://docs.python.org/2/library/os.html#os.stat
        # ino = windows does not return an inode number
        # size = size in bytes (long)  ... special files = amount of data waiting
        # atime = last access time , mtime = last modified time , ctime = creation time (windows) OR last metadata change time (linux)
        """
        statinfo = os.stat(self.full_path)
        results = None
        self.size = statinfo.st_size
        self.last_modified = statinfo.st_mtime

    def get_image_info(self):
        """ https://pillow.readthedocs.org/reference/ExifTags.html?highlight=exif#module-PIL.ExifTags
            http://www.w3.org/2003/12/exif/
        :return: a dictionary containing the EXIF info
        """
        info = dict()
        image = Image.open(self.full_path)
        try:
            exif_data = image._getexif()
            if exif_data != None:
                for tag, value in exif_data.items():
                    decoded = TAGS.get(tag, tag)
                    info[decoded] = value
        except Exception as error:
            print error
        return info

    @staticmethod
    def determine_os():
        if sys.platform.startswith('linux'):
            return 'LINUX'
        elif sys.platform.startswith('darwin'):
            return 'MAC'
        elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
            return 'WINDOWS'
        else:
            return 'OTHER'


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', help='path to a directory of file(s)', required=True)
    args = parser.parse_args()
    # print FileInfo.determine_os()
    for file_name in os.listdir(args.path):
        file_full_path = os.path.join(args.path, file_name)
        f_info = FileInfo(file_full_path)
        # f_info.discover_file_system_info()
        # print f_info.size  # bytes
        # print f_info.last_modified  # epoch seconds
        image_info = f_info.get_image_info()
        # print image_info
        image_date_time_original = image_info.get('DateTimeOriginal')
        print file_name, image_date_time_original
        if image_date_time_original:
            new_name = image_date_time_original.replace(':', '-')
            new_name = new_name.replace(' ', '--')
            print new_name
            new_file_full_path = os.path.join(args.path, new_name)
            print new_file_full_path
            os.rename(file_full_path, new_file_full_path)