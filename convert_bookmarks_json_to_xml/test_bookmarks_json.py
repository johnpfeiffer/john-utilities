#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import unittest

from bookmarks_json import Node, MyJSONTreeParser, FOLDER_TYPE, URL_TYPE


class TestNodeTree(unittest.TestCase):

    def setUp(self):
        self.parser = MyJSONTreeParser()

    def test_empty_node(self):
        self.parser.source_json_string = '{"bookmark_bar": {"name": "Bookmarks bar"}}'
        self.parser.parse()
        # Then
        self.assertEqual(FOLDER_TYPE, self.parser.root.type)
        self.assertEqual(1, len(self.parser.root.folders))
        node = self.parser.root.folders[0]
        self.assertEqual('Bookmarks bar', node.name)
        self.empty_node_assertions(node)
        expected_node = Node()
        expected_node.name = 'Bookmarks bar'
        self.assertTrue(node.is_equal(expected_node))

    def test_two_empty_bookmark_folders(self):
        expected_empty = Node()
        expected_empty.name = 'Bookmarks bar'
        expected_other = Node()
        expected_other.name = 'Other bookmarks'
        expected_other.date_added = '12963388334079039'
        expected_other.date_modified = '13073675893574358'
        expected_other.folder_id = '2'
        expected_nodes = {'empty': expected_empty, 'other': expected_other}
        self.parser.source_json_string = '{"bookmark_bar": {"name": "Bookmarks bar"}, "other": {"children": [  ], ' \
                                         '"date_added": "12963388334079039", "date_modified": "13073675893574358", ' \
                                         '"id": "2", "name": "Other bookmarks", "type": "folder"}}'
        self.parser.parse()
        # Then
        self.assertEqual(FOLDER_TYPE, self.parser.root.type)
        self.assertEqual(2, len(self.parser.root.folders))
        node = self.parser.root.folders.pop()
        self.assertEqual(FOLDER_TYPE, node.type)
        if node.name == 'Other bookmarks':
            expected = expected_nodes.pop('other')
        else:
            expected = expected_nodes.pop('empty')
            self.empty_node_assertions(node)
        self.assertTrue(node.is_equal(expected))

        node = self.parser.root.folders.pop()
        self.assertEqual(FOLDER_TYPE, node.type)
        k, expected = expected_nodes.popitem()
        self.assertTrue(node.is_equal(expected))
        self.assertEqual(0, len(self.parser.root.folders))


# Test Helpers

    def empty_node_assertions(self, node):
        self.assertEqual(FOLDER_TYPE, node.type)
        self.assertIsNone(node.date_added)
        self.assertIsNone(node.date_modified)
        self.assertIsNone(node.folder_id)
        self.assertEqual(0, len(node.folders))
