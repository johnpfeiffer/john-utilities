#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import sys
import json

FOLDER_TYPE = 'folder'
URL_TYPE = 'url'


class Node(object):

    def __init__(self):
        self.folders = list()
        self.urls = list()
        self.date_added = None
        self.date_modified = None
        self.folder_id = None
        self.name = None
        self.type = FOLDER_TYPE

    def is_equal(self, other):
        result = False
        if self.type == other.type and self.name == other.name and self.folder_id == other.folder_id and \
            self.date_modified == other.date_modified and self.date_added == other.date_added and self.urls == \
                other.urls and self.folders == other.folders:
            result = True
        return result


class MyJSONTreeParser(object):
    def __init__(self, source_json=None):
        self.source_json_string = source_json
        self.root = Node()

    def parse(self):
        json_object = json.loads(self.source_json_string)
        for k, v in json_object.items():
            n = Node()
            n.name = v.get('name')
            n.date_added = v.get('date_added', None)
            n.date_modified = v.get('date_modified', None)
            n.folder_id = v.get('id', None)
            for c in v.get('children', list()):
                n.folders.append(c)
            self.root.folders.append(n)


if __name__ == '__main__':
    json_file_path = sys.argv[1]
    with open(json_file_path) as f:
        json_string = f.read()
        m = MyJSONTreeParser(source_json=json_string)
        m.parse()
