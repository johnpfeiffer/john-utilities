#!/usr/bin/env python

import smtpd
import asyncore
# https://docs.python.org/2/library/smtpd.html
# sudo python -m smtpd -n -c DebuggingServer localhost:25
# "Messages will be discarded, and printed on stdout."


class EmailSMTPServer(smtpd.SMTPServer):

    # def process_message(self, peer, mailfrom, rcpttos, data):
        # print(repr(peer))
        # print(repr(mailfrom))
        # print(repr(rcpttos))
        # print(repr(data))
    def process_message(*args, **kwargs):
        pass

if __name__ == "__main__":
    # 0.0.0.0 allows all client connections, change it to localhost for local development machine access only
    smtp_server = EmailSMTPServer(('0.0.0.0', 2525), None)
    try:
        asyncore.loop()
    except KeyboardInterrupt:
        smtp_server.close()
