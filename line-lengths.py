import sys
import os
import time


def get_line_lengths( filepath ):
    line_lengths = dict()
    
    file = open( file_name )
    for line in file:
        if line and line.strip():
            length = len( line )
            current = line_lengths.get( line ):
            if current:
                current += 1
            else:
                current = 1
            line_lengths[ length ] = current
    return line_lengths



def walk_create_hashes( start, extension=None ):
    """ Walk a file tree to populate a dictionary of line counts (ignoring whitespace lines)
    """
    file_count = 0
    total_lines_count = 0
    line_lengths = dict()
    for dirpath, directorynames, filenames in os.walk( start ):
        for f in filenames:
            file_count += 1
            search = True
            if extension and not f.endswith( extension ):
                search =  False
            if search:
                full_path = os.path.join( dirpath, f )
                file_hash = get_file_hash( full_path, hashlib.sha1() )

                duplicates = hashes.get( file_hash )
                if not duplicates:
                    duplicates = list()
                duplicates.append( full_path )
                hashes[ file_hash ] = duplicates
                total_hash_count += 1

    return file_count, total_hash_count, hashes


def count_uniques( hashes ):
    count = 0
    for key, value in hashes.iteritems():
        count += 1
    return count


def find_duplicates( hashes ):
    count = 0
    for key, value in hashes.iteritems():
        if len( value ) > 1:        # list of filenames
            print key, repr( value )
            count += 1
    return count




# TODO: later adding sha256 and size comparison in a "more accurate" mode
if __name__ == '__main__':
    if len( sys.argv ) < 2:
        print "correct usage: python hash-duplicate.py startdirectory [extension] (e.g. python hash-duplicate.py /tmp .txt)"
        sys.exit( 1 )
    startpath = os.path.normpath( sys.argv[1] )
    if len( sys.argv ) == 3:
        extension = sys.argv[2]
    else:
        extension = None

    print "searching for duplicates in: ", startpath,
    if extension:
        print "filtered by:", extension,
    print

    start_time = time.time()
    file_count, hash_count, hashes = walk_create_hashes( startpath, extension )
    count = find_duplicates( hashes )
    print count, " duplicates"
    unique_hash_count = count_uniques( hashes )
    elapsed_time = time.time() - start_time
    print 'searched: {0} files, created: {1} hashes, found: {2} unique hashes, in {3:.2f} seconds'.format(
        file_count, hash_count, unique_hash_count, elapsed_time )