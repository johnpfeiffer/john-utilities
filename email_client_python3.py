#!/usr/bin/env python3

# python3 -m smtpd -n -c DebuggingServer localhost:2525
# python3 email_client_python3.py
# https://docs.python.org/3/library/smtplib.html
# https://docs.python.org/3/library/email-examples.html


import smtplib
from email.mime.text import MIMEText

msg = MIMEText('this is the message body')
msg['Subject'] = 'The subject of my email'
msg['From'] = 'john@example.com'
msg['To'] = 'me@example.com'
s = smtplib.SMTP(host='localhost', port=2525)
s.send_message(msg)
s.quit()

# Check if the server is running/accessible and port number: [Errno 111] Connection refused