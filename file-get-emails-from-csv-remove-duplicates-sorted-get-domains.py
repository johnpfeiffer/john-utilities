emails = list()
filename = '/tmp/source.txt'
with open(filename) as f:
  for line in f:
    if '@' in line:
      # pieces = line.split(',')
      # for p in pieces:
      #   if '@' in p:
      emails.append(line.strip())
print len(emails)  # sanity check


emails_set = set(emails)  # emails_set.update(listname)
print len(emails_set)     # duplicates?

temp_outfile = '/tmp/output.txt'
with open(temp_outfile, 'w') as f:
  for email in sorted(list(emails_set)):
    f.write(email + '\n')   


- - - - - - - - - - - - - - - - - - - - - - - -
email_parts = list()
for i in emails_set:
  email_parts.append(i.split('@'))
print len(email_parts)  # sanity check

domains_and_emails = list()
for item in email_parts:
  email = item[0]  
  domain = item[1]
  domains_and_emails.append('{}:{}@{}'.format(domain,email,domain))

domains_and_emails.sort()
print len(domains_and_emails)  # sanity check

with open('/tmp/emails2.txt', 'w') as f:
  for item in domains_and_emails:
    parts = item.split(':')
    f.write('{}\n'.format(parts[1]))
  
# domains = set()
# for item in email_parts:
#  domains.add(item[1])


- - - - - - - - - - - - - - - - - - - - - - - -
#!/usr/bin/python
# 2012-12-17 johnpfeiffer


def csvRead( file_name ):
  file = open( file_name )
  line_list = list()

  for line in file:
# DEBUG    print line
    line = line.strip()
    if not line:
      continue	# skip empty lines
    line_array = line.split( ',' )
    line_list.append( line_array )    

  file.close()
  return line_list


def extractEmailAddress( line_list ):
  email_list = list()
  for line_array in line_list:
    for element in line_array:
      if "@" in element:
        email_list.append( element )
# DEBUG    print line_array
  return email_list  

def removeDuplicates( source_list ):
  return list( set( source_list ) )


# main #####

def main():

  file_name = '2012-christmas-list.txt'
  line_list = csvRead( file_name )
  email_list = sorted( removeDuplicates( extractEmailAddress( line_list ) ) )

  for email_address in email_list:
    print email_address



if __name__ == "__main__":
     main()

